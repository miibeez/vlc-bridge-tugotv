# vlc-bridge-tugotv

watch tugo.tv live stream in VLC

### using

`$ docker run -d -e 'TUGOTV_USER=user@email.com' -e 'TUGOTV_PASS=secret' -p 7777:7777 --name vlc-bridge-tugotv registry.gitlab.com/miibeez/vlc-bridge-tugotv`

`$ vlc http://localhost:7777/tugotv/playlist.m3u`

### require

tugo tv subscription
