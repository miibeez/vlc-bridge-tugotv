import json
import os
import sys
import time
import random
import threading
import requests
import jwt
import uuid

if os.environ.get("TUGOTV_USER") is None or os.environ.get("TUGOTV_PASS") is None:
    sys.stderr.write("TUGOTV_USER and TUGOTV_PASS need set\n")
    sys.exit(1)

class Client:
    def __init__(self):
        self.user = os.environ["TUGOTV_USER"]
        self.passwd = os.environ["TUGOTV_PASS"]

        self.device = None
        self.session = None
        self.mutex = threading.Lock()

        self.load_device()

    def channels(self):
        response = requests.get("https://api.us.tugotv.com/client/api/servicespec/5f69e0c785c03b000159f5f9/tv/channels")
        channels = response.json()
        stations = []
        for ch in channels:
            if ch["status"] != "ACTIVE":
                continue
            logo = ""
            for l in ch["logos"]:
                logo = "https://img.tugotv.com/images/" + l["path"]
                break
            stations.append({"id": ch["_id"], "name": ch["title"], "logo": logo})
        return stations, None

    def watch(self, id):
        auth, err = self.auth_token()
        if err:
            return None, err

        headers = {
            "Authorization": f"JWT {auth}",
            "Accept-Media-Stream": "MPEG_DASH,HLS,MP3,MP4,AAC,OGG,FLAC",
            "Accept-Media-Stream-Encrypted": "MPEG_DASH/WIDEVINE",
            "Origin": "https://us.tugotv.com"
        }
        data = {"id": id}
        response = requests.post("https://priv-api.us.tugotv.com/client/api/v3/tv/channels/sources", headers=headers, json=data)
        streams = response.json()

        if not streams:
            return None, "no streams"
        s = streams[id]
        if not s:
            return None, "no sources"
        for st in s:
            if st["type"] == "HLS":
                for src in st["sources"]:
                    if src["protocol"] == "HLS":
                        return src["src"], None
        return None, "no video"

    def load_device(self):
        with self.mutex:
            try:
                with open("tugotv-device.json", "r") as f:
                    self.device = json.load(f)
            except FileNotFoundError:
                self.device = {
                    "_id": str(uuid.uuid4()),
                    "type": "PC",
                    "model": "Gecko",
                    "platform": "BROWSER",
                    "brand": "Google Inc.",
                    "serial": random_mac_address().replace(":", ""),
                    "mac": random_mac_address()
                }
                with open("tugotv-device.json", "w") as f:
                    json.dump(self.device, f)

    def login(self):
        with self.mutex:
            data = {
                "client_id": "Ok8Bc3fsJ4Dg0dh6bDH9bd8wg1gcet",
                "client_secret": "PodF2Bfe4D6Bfbdi4uYt6ee9DURIfy7IguIgRd",
                "device": self.device,
                "username": self.user,
                "password": self.passwd,
                "grant_type": "password"
            }
            response = requests.post("https://priv-api.us.tugotv.com/client/auth/token", json=data)
            self.session = response.json()
        if response.status_code != 200:
            return response.text

    def auth_token(self):
        if self.session and not jwt_expired(self.session["access_token"]):
            return self.session["access_token"], None

        err = self.login()
        if err:
            return None, err

        return self.session["access_token"], None

def random_mac_address():
    bytes = [random.randint(0, 255) for _ in range(6)]
    return "{:02X}:{:02X}:{:02X}:{:02X}:{:02X}:{:02X}".format(*bytes)

def jwt_expired(jwt_token):
    decoded = jwt.decode(jwt_token, verify=False)
    return decoded["exp"] < time.time()
